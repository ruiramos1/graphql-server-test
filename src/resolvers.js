module.exports = {
  Query: {
    cases: (parent, args, context, info) =>
      context.dataSources.casesAPI.getCases(args.from),
    deaths: (parent, args, context, info) =>
      context.dataSources.casesAPI.getDeaths(args.from),
  },
};
