const { gql } = require("apollo-server");

const typeDefs = gql`
  type Query {
    cases(from: String): [Case]
    deaths(from: String): [Death]
  }

  type Case {
    date: String!
    dailyCases: Int
    cumCases: Int
  }

  type Death {
    date: String!
    dailyDeaths: Int
    cumDeaths: Int
  }
`;

module.exports = typeDefs;
