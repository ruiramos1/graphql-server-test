const { RESTDataSource } = require("apollo-datasource-rest");

class CasesAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL =
      "https://services1.arcgis.com/0IrmI40n5ZYxTUrV/arcgis/rest/services/DailyConfirmedCases/FeatureServer/0/";
  }

  async getCases(from) {
    const response = await this.get(
      "query",
      {
        f: "pjson",
        where: "1=1",
        outFields: ["DateVal", "CMODateCount", "CumCases"],
      },
      {
        cacheOptions: {
          ttl: -1,
        },
      }
    );

    let obj = JSON.parse(response);
    return this.caseReducer(obj.features);
  }

  caseReducer(cases) {
    return cases.map((day) => ({
      date: String(day.attributes.DateVal),
      dailyCases: day.attributes.CMODateCount || 0,
      cumCases: day.attributes.CumCases || 0,
    }));
  }

  async getDeaths(from) {
    const response = await this.get(
      "query",
      {
        f: "pjson",
        where: `1=1`,
        outFields: ["DateVal", "CumDeaths", "DailyDeaths"],
      },
      {
        cacheOptions: {
          ttl: -1,
        },
      }
    );

    let obj = JSON.parse(response);
    return this.deathReducer(obj.features);
  }

  deathReducer(cases) {
    return cases.map((day) => ({
      date: String(day.attributes.DateVal),
      cumDeaths: day.attributes.CumDeaths || 0,
      dailyDeaths: day.attributes.DailyDeaths || 0,
    }));
  }
}

module.exports = CasesAPI;
